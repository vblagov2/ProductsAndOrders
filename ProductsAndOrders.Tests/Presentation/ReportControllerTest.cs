using AutoFixture.Xunit2;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using ProductsAndOrders.Controllers;
using ProductsAndOrders.Models.BusinessModels;
using ProductsAndOrders.Models.PresentationModels;
using ProductsAndOrders.Services;
using System.Threading.Tasks;
using Xunit;

namespace ProductsAndOrders.Tests
{
    public class ReportControllerTest
    {
        [Theory, AutoMoqData]
        public async Task GetMostSealeableProductAsync_Successfully([Frozen]Mock<IReportsService> reportsServiceMock,
            [Frozen]Mock<IMapper> mapper, BusinessProduct businessProduct, ReportsController sut,
            PresentationProduct presentationProduct)
        {
            // Arrange
            reportsServiceMock.Setup(svc => svc.GetMostSaleableProductAsync())
                .ReturnsAsync(businessProduct);
            mapper.Setup(svc => svc.Map<PresentationProduct>(businessProduct))
                .Returns(presentationProduct);

            // Act
            var result = await sut.GetMostSaleableProductAsync() as OkObjectResult;

            // Assert
            Assert.Equal(result.Value, presentationProduct);
        }

        [Theory, AutoMoqData]
        public async Task GetMostExpensiveOrderAsync_Successfully([Frozen]Mock<IReportsService> reportsServiceMock,
            [Frozen]Mock<IMapper> mapper, BusinessOrder businessOrder, ReportsController sut,
            PresentationOrder presentationOrder)
        {
            // Arrange
            reportsServiceMock.Setup(svc => svc.GetMostExpensiveOrderAsync())
                .ReturnsAsync(businessOrder);
            mapper.Setup(svc => svc.Map<PresentationOrder>(businessOrder))
                .Returns(presentationOrder);

            // Act
            var result = await sut.GetMostExpensiveOrderAsync() as OkObjectResult;

            // Assert
            Assert.Equal(result.Value, presentationOrder);
        }

    }
}