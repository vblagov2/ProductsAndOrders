using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using ProductsAndOrders.Controllers;
using ProductsAndOrders.Models;
using ProductsAndOrders.Models.BusinessModels;
using ProductsAndOrders.Models.PresentationModels;
using ProductsAndOrders.Models.Requests;
using ProductsAndOrders.Services;
using Xunit;

namespace ProductsAndOrders.Tests
{
    public class OrderControllerTest
    {
        [Theory, AutoMoqData]
        public async Task GetOrdersAsync_Successfully([Frozen]Mock<IOrdersService> ordersServiceMock,
            SearchOrderQuery searchOrderQuery, [Frozen]Mock<IMapper> mapper, BusinessSearchOrder businessSearchOrder,
            OrdersController sut, List<PresentationOrder> presentationOrders, List<BusinessOrder> businessOrders)
        {
            // Arrange
            mapper.Setup(svc => svc.Map<BusinessSearchOrder>(searchOrderQuery))
                .Returns(businessSearchOrder);
            ordersServiceMock.Setup(svc => svc.GetListAsync(businessSearchOrder))
                .ReturnsAsync(businessOrders);
            mapper.Setup(svc => svc.Map<List<PresentationOrder>>(businessOrders))
                .Returns(presentationOrders);

            // Act
            var result = await sut.GetAsync(searchOrderQuery) as OkObjectResult;

            // Assert
            Assert.Equal(result.Value, presentationOrders);
        }

        [Theory, AutoMoqData]
        public async Task GetOrderByIdAsync_Successfully([Frozen]Mock<IOrdersService> ordersServiceMock,
            int id, BusinessOrder businessOrder, [Frozen]Mock<IMapper> mapper, OrdersController sut,
            PresentationOrder presentationOrder)
        {
            // Arrange
            ordersServiceMock.Setup(svc => svc.GetAsync(id))
                .ReturnsAsync(businessOrder);
            mapper.Setup(svc => svc.Map<PresentationOrder>(businessOrder))
                .Returns(presentationOrder);

            // Act
            var result = await sut.GetAsyncById(id) as OkObjectResult;

            // Assert
            Assert.Equal(result.Value, presentationOrder);
        }

        [Theory, AutoMoqData]
        public async Task UpdateOrderAsync_Successfully([Frozen]Mock<IOrdersService> ordersServiceMock,
            [Frozen]Mock<IMapper> mapperMock, BusinessOrder businessOrder,
            int id, UpdateOrderRequest request, PresentationOrder presentationOrder,
            OrdersController sut)
        {
            // Arrange
            businessOrder.OrderId = id;
            mapperMock.Setup(svc => svc.Map<BusinessOrder>(request))
                .Returns(businessOrder);
            ordersServiceMock.Setup(svc => svc.UpdateAsync(businessOrder))
                .ReturnsAsync(businessOrder);
            mapperMock.Setup(svc => svc.Map<PresentationOrder>(businessOrder))
                .Returns(presentationOrder);

            // Act
            var result = await sut.UpdateAsync(id, request) as OkObjectResult;

            // Assert
            Assert.Equal(result.Value, presentationOrder);
        }

        [Theory, AutoMoqData]
        public async Task AddOrderAsync_Successfully([Frozen]Mock<IOrdersService> ordersServiceMock,
            AddOrderRequest request, [Frozen]Mock<IMapper> mapper,
            OrdersController sut, BusinessOrder businessOrder)
        {
            // Arrange
            mapper.Setup(svc => svc.Map<BusinessOrder>(request))
                .Returns(businessOrder);

            // Act
            await sut.AddAsync(request);

            // Assert
            ordersServiceMock.Verify(svc => svc.AddAsync(businessOrder), Times.Once);
        }

        [Theory, AutoMoqData]
        public async Task DeleteOrderAsync_Successfully([Frozen]Mock<IOrdersService> ordersServiceMock,
            int id, OrdersController sut)
        {
            // Arrange

            // Act
            await sut.DeleteAsync(id);

            // Assert
            ordersServiceMock.Verify(svc => svc.DeleteAsync(id));
        }
    }
}