using System.Collections.Generic;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Moq;
using ProductsAndOrders.Controllers;
using ProductsAndOrders.Models;
using ProductsAndOrders.Models.BusinessModels;
using ProductsAndOrders.Models.PresentationModels;
using ProductsAndOrders.Models.Requests;
using ProductsAndOrders.Services;
using Xunit;

namespace ProductsAndOrders.Tests.Presentation
{
    public class ProductControllerTest
    {

        [Theory, AutoMoqData]
        public async Task GetProductsAsync_Successfuly([Frozen]Mock<IProductsService> productsServiceMock, [Frozen]Mock<IMapper> mapperMock,
            SearchProductQuery searchProductQuery, BusinessSearchProduct businessSearchProduct, List<BusinessProduct> products, 
            List<PresentationProduct> presentationProducts, ProductsController sut)
        {
            // Arrange
            mapperMock.Setup(svc => svc.Map<BusinessSearchProduct>(searchProductQuery))
                .Returns(businessSearchProduct);
            productsServiceMock.Setup(svc => svc.GetListAsync(businessSearchProduct))
                .ReturnsAsync(products);
            mapperMock.Setup(svc => svc.Map<List<PresentationProduct>>(products))
                .Returns(presentationProducts);

            // Act
            var result = await sut.GetAsync(searchProductQuery) as OkObjectResult;

            // Assert
            Assert.Equal(result.Value, presentationProducts);
        }

        [Theory, AutoMoqData]
        public async Task GetProductByIdAsync_Successfully([Frozen]Mock<IProductsService> productsServiceMock, [Frozen]Mock<IMapper> mapperMock,
            int id, ProductsController sut, BusinessProduct businessProduct, PresentationProduct presentationProduct)
        {
            // Arrange
            productsServiceMock.Setup(svc => svc.GetAsync(id)).ReturnsAsync(businessProduct);
            mapperMock.Setup(svc => svc.Map<PresentationProduct>(businessProduct))
                .Returns(presentationProduct);

            // Act
            var result = await sut.GetAsyncById(id) as OkObjectResult;

            // Assert
            Assert.Equal(result.Value, presentationProduct);
        }


        [Theory, AutoMoqData]
        public async Task UpdateProductAsync_Successfully([Frozen]Mock<IProductsService> productsServiceMock,
            [Frozen]Mock<IMapper> mapperMock, BusinessProduct businessProduct, PresentationProduct presentationProduct,
            int id, UpdateProductRequest request, ProductsController sut)
        {
            // Arrange
            businessProduct.ProductId = id;
            mapperMock.Setup(svc => svc.Map<BusinessProduct>(request))
                .Returns(businessProduct);
            productsServiceMock.Setup(svc => svc.UpdateAsync(businessProduct))
                .ReturnsAsync(businessProduct);
            mapperMock.Setup(svc => svc.Map<PresentationProduct>(businessProduct))
                .Returns(presentationProduct);

            // Act
            var result = await sut.UpdateAsync(id, request) as OkObjectResult;

            // Assert
            Assert.Equal(result.Value, presentationProduct);
        }

        [Theory, AutoMoqData]
        public async Task AddProductAsync_Successfully([Frozen]Mock<IProductsService> productsServiceMock,
            [Frozen]Mock<IMapper> mapper, AddProductRequest request,
            ProductsController sut, BusinessProduct businessProduct)
        {
            // Arrange
            mapper.Setup(svc => svc.Map<BusinessProduct>(request))
                .Returns(businessProduct);

            // Act
            var result = await sut.AddAsync(request);

            // Assert
            productsServiceMock.Verify(svc => svc.AddAsync(businessProduct), Times.Once);
        }

        [Theory, AutoMoqData]
        public async Task DeleteProductAsync_Successfully([Frozen]Mock<IProductsService> productsServiceMock,
            int id, ProductsController sut)
        {
            // Arrange

            // Act
            var result = await sut.DeleteAsync(id);

            // Assert
            productsServiceMock.Verify(svc => svc.DeleteAsync(id), Times.Once);
        }
    }
}