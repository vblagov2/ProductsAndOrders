using AutoFixture;
using AutoFixture.AutoMoq;
using AutoFixture.Xunit2;

namespace ProductsAndOrders.Tests
{
    public class AutoMoqDataAttribute : AutoDataAttribute
    {
        public AutoMoqDataAttribute() : base(() =>
            new Fixture()
                .Customize(new AutoMoqCustomization { ConfigureMembers = true })
                .Customize(new OrdersProductsCustomization()))
        {

        }


    }
}