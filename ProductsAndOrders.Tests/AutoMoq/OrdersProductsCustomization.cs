using AutoFixture;
using ProductsAndOrders.Models;

namespace ProductsAndOrders.Tests
{
    public class OrdersProductsCustomization : ICustomization
    {
        public void Customize(IFixture fixture)
        {
            fixture.Customize<Products>(c => c
                .Without(x => x.OrdersProducts));
            fixture.Customize<Orders>(c => c
                .Without(x => x.OrdersProducts));
        }
    }
}