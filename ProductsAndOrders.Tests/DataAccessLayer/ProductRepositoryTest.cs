using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using ProductsAndOrders.DataStorage;
using ProductsAndOrders.Exceptions;
using ProductsAndOrders.Models.DTOModels;
using ProductsAndOrders.Models.Exceptions;
using ProductsAndOrders.Tests.DataAccessLayer.DbSetMocks;
using Xunit;

namespace ProductsAndOrders.Tests
{
    public class ProductsRepositoryTests
    {
        [Theory, AutoMoqData]
        public async Task GetAsync_Returns_All_Products(Mock<LibraryContext> context, ProductsLibraryRepository repo)
        {
            //Arrange
            var products = new List<Products> { new Products { IsDeleted = false} };
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);

            context.Setup(svc => svc.Products).Returns(productsDbSet);
            repo.SetContext(context.Object);

            // Act
            var result = await repo.GetAsync();

            //Assert
            Assert.Single(result);
        }


        [Theory, AutoMoqData]
        public async Task GetByIdAsync_Returns_Product(Mock<LibraryContext> context, int id, ProductsLibraryRepository repo)
        {
            //Arrange
            var product = new Products { IsDeleted = false, ProductId = id };
            var products = new List<Products> { product };
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);

            context.Setup(svc => svc.Products).Returns(productsDbSet);
            repo.SetContext(context.Object);

            // Act
            var result = await repo.GetAsync(id);

            //Assert
            Assert.Equal(result, product);
        }

        [Theory, AutoMoqData]
        public async Task GetByIdAsync_Throws_ItemNotFoundException(Mock<LibraryContext> context, int id, ProductsLibraryRepository repo)
        {
            //Arrange
            var entityName = "Item";
            var product = new Products { IsDeleted = true, ProductId = id };
            var products = new List<Products> { product };
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);

            context.Setup(svc => svc.Products).Returns(productsDbSet);
            repo.SetContext(context.Object);

            //Act
            Func<Task> act = async () => await repo.GetAsync(id);

            //Assert
            Exception result = await Assert.ThrowsAsync<ItemNotFoundException>(act);
            Assert.Equal(string.Format(ErrorMessageConsts.NotFound, entityName, id), result.Message);
        }

        [Theory, AutoMoqData]
        public async Task UpdateAsync_Successfully(Mock<LibraryContext> context, Mock<IMapper> mapperMock, ProductToUpdate productToUpdate,
            Products product)
        {
            //Arrange
            product.IsDeleted = false;
            product.ProductId = productToUpdate.ProductId;
            var products = new List<Products> { product };
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);

            context.Setup(svc => svc.Products).Returns(productsDbSet);
            mapperMock.Setup(svc => svc.Map(productToUpdate, product))
                .Returns(new Products { Price = productToUpdate.Price});

            var repo = new ProductsLibraryRepository(mapperMock.Object);
            repo.SetContext(context.Object);

            // Act
            var result = await repo.UpdateAsync(productToUpdate);

            //Assert
            Assert.Equal(result.Price, productToUpdate.Price);
        }

        [Theory, AutoMoqData]
        public async Task UpdateAsync_Throws_ItemNotFoundException(Mock<LibraryContext> context, ProductToUpdate productToUpdate,
            Products product, ProductsLibraryRepository repo)
        {
            //Arrange
            var entityName = "Item";
            product.IsDeleted = false;
            var products = new List<Products> { product };
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);

            context.Setup(svc => svc.Products).Returns(productsDbSet);

            repo.SetContext(context.Object);

            //Act
            Func<Task> act = async () => await repo.UpdateAsync(productToUpdate);

            //Assert
            Exception result = await Assert.ThrowsAsync<ItemNotFoundException>(act);
            Assert.Equal(string.Format(ErrorMessageConsts.NotFound, entityName, productToUpdate.ProductId), result.Message);
        }


        [Theory, AutoMoqData]
        public async Task AddAsync_Successfully(Mock<LibraryContext> context, Products product, Products newProduct,
            ProductsLibraryRepository repo)
        {
            // Arrange
            var products = new List<Products> { product };
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);

            context.Setup(svc => svc.Products).Returns(productsDbSet);
            repo.SetContext(context.Object);

            // Act
            await repo.AddAsync(newProduct);

            // Assert
            context.Verify(x => x.Products.AddAsync((newProduct), It.IsAny<CancellationToken>()), Times.Once);
        }

        [Theory, AutoMoqData]
        public async Task DeleteAsync_Successfully(Mock<LibraryContext> context, int id, Products product, ProductsLibraryRepository repo)
        {
            // Arrange
            product.ProductId = id;
            product.IsDeleted = false;
            var products = new List<Products> { product };
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);

            context.Setup(svc => svc.Products).Returns(productsDbSet);
            repo.SetContext(context.Object);

            // Act
            await repo.DeleteAsync(id);

            // Assert
            context.Verify(x => x.Products.Remove(It.IsAny<Products>()), Times.Once);
        }

        [Theory, AutoMoqData]
        public async Task DeleteAsync_Throws_ItemNotFoundException(Mock<LibraryContext> context, int id, Products product, ProductsLibraryRepository repo)
        {
            // Arrange
            var entityName = "Item";
            product.ProductId = id;
            product.IsDeleted = true;
            var products = new List<Products> { product };
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);

            context.Setup(svc => svc.Products).Returns(productsDbSet);
            repo.SetContext(context.Object);

            //Act
            Func<Task> act = async () => await repo.DeleteAsync(id);

            //Assert
            Exception result = await Assert.ThrowsAsync<ItemNotFoundException>(act);
            Assert.Equal(string.Format(ErrorMessageConsts.NotFound, entityName, id), result.Message);
        }

    }
}