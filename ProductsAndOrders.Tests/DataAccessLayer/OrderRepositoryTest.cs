using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Moq;
using ProductsAndOrders.DataStorage;
using ProductsAndOrders.Exceptions;
using ProductsAndOrders.Models;
using ProductsAndOrders.Models.DTOModels;
using ProductsAndOrders.Models.Exceptions;
using ProductsAndOrders.Tests.DataAccessLayer.DbSetMocks;
using Xunit;

namespace ProductsAndOrders.Tests.DataAccessLayer
{
    public class OrderRepositoryTest
    {
        [Theory, AutoMoqData]
        public async Task GetAsync_Successfully(Mock<LibraryContext> context, OrdersLibraryRepository repo,
            Orders order, Products product)
        {
            //Arrange
            var ordersProduct = new OrdersProducts
            {
                Order = order,
                Product = product,
                OrderId = order.OrderId,
                ProductId = product.ProductId
            };

            var orders = new List<Orders> { order };
            var products = new List<Products> { product };
            var ordersProducts = new List<OrdersProducts> { ordersProduct };

            var ordersDbSet = FakeDbSet.GetQueryableMockDbSet(orders);
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);
            var ordersProductsDbSet = FakeDbSet.GetQueryableMockDbSet(ordersProducts);

            context.Setup(svc => svc.Orders).Returns(ordersDbSet);
            context.Setup(svc => svc.Products).Returns(productsDbSet);
            context.Setup(svc => svc.OrdersProducts).Returns(ordersProductsDbSet);
            repo.SetContext(context.Object);

            // Act
            var result = await repo.GetAsync();

            //Assert
            Assert.Single(result);
        }

        [Theory, AutoMoqData]
        public async Task GetByIdAsync_Returns_OrderDTO(Mock<LibraryContext> context, OrdersLibraryRepository repo,
            Orders order, Products product)
        {
            //Arrange
            var ordersProduct = new OrdersProducts
            {
                Order = order,
                Product = product,
                OrderId = order.OrderId,
                ProductId = product.ProductId
            };

            var orders = new List<Orders> { order };
            var products = new List<Products> { product };
            var ordersProducts = new List<OrdersProducts> { ordersProduct };

            var ordersDbSet = FakeDbSet.GetQueryableMockDbSet(orders);
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);
            var ordersProductsDbSet = FakeDbSet.GetQueryableMockDbSet(ordersProducts);

            context.Setup(svc => svc.Orders).Returns(ordersDbSet);
            context.Setup(svc => svc.Products).Returns(productsDbSet);
            context.Setup(svc => svc.OrdersProducts).Returns(ordersProductsDbSet);
            repo.SetContext(context.Object);

            // Act
            var result = await repo.GetAsync(order.OrderId);

            //Assert
            Assert.Equal(result.DestinationCity, order.DestinationCity);
            Assert.Equal(result.RecipientName, order.RecipientName);
        }

        [Theory, AutoMoqData]
        public async Task GetByIdAsync_Throws_ItemNotFoundException(Mock<LibraryContext> context, OrdersLibraryRepository repo, int id,
            Orders order, Products product)
        {
            //Arrange
            var entityName = "Item";
            var ordersProduct = new OrdersProducts
            {
                Order = order,
                Product = product,
                OrderId = order.OrderId,
                ProductId = product.ProductId
            };

            var orders = new List<Orders> { order };
            var products = new List<Products> { product };
            var ordersProducts = new List<OrdersProducts> { ordersProduct };

            var ordersDbSet = FakeDbSet.GetQueryableMockDbSet(orders);
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);
            var ordersProductsDbSet = FakeDbSet.GetQueryableMockDbSet(ordersProducts);

            context.Setup(svc => svc.Orders).Returns(ordersDbSet);
            context.Setup(svc => svc.Products).Returns(productsDbSet);
            context.Setup(svc => svc.OrdersProducts).Returns(ordersProductsDbSet);
            repo.SetContext(context.Object);

            //Act
            Func<Task> act = async () => await repo.GetAsync(id);

            //Assert
            Exception result = await Assert.ThrowsAsync<ItemNotFoundException>(act);
            Assert.Equal(string.Format(ErrorMessageConsts.NotFound, entityName, id), result.Message);
        }

        [Theory, AutoMoqData]
        public async Task Update_Throws_ItemNotFoundException(Mock<LibraryContext> context, Mock<IMapper> mapperMock, Orders order,
            OrderToUpdate orderToUpdate, Products product)
        {
            //Arrange
            var entityName = "Item";
            var ordersProduct = new OrdersProducts
            {
                Order = order,
                Product = product,
                OrderId = order.OrderId,
                ProductId = product.ProductId
            };

            var orders = new List<Orders> { order };
            var products = new List<Products> { product };
            var ordersProducts = new List<OrdersProducts> { ordersProduct };

            var ordersDbSet = FakeDbSet.GetQueryableMockDbSet(orders);
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);
            var ordersProductsDbSet = FakeDbSet.GetQueryableMockDbSet(ordersProducts);

            context.Setup(svc => svc.Orders).Returns(ordersDbSet);
            context.Setup(svc => svc.Products).Returns(productsDbSet);
            context.Setup(svc => svc.OrdersProducts).Returns(ordersProductsDbSet);

            var repo = new OrdersLibraryRepository(mapperMock.Object);
            repo.SetContext(context.Object);

            //Act
            Func<Task> act = async () => await repo.GetAsync(orderToUpdate.OrderId);

            //Assert
            Exception result = await Assert.ThrowsAsync<ItemNotFoundException>(act);
            Assert.Equal(string.Format(ErrorMessageConsts.NotFound, entityName, orderToUpdate.OrderId), result.Message);
        }

        [Theory, AutoMoqData]
        public async Task Update_Successfully(Mock<LibraryContext> context, Mock<IMapper> mapperMock, Orders order,
            OrderToUpdate orderToUpdate, Products product)
        {
            //Arrange
            order.OrderId = orderToUpdate.OrderId;
            var ordersProduct = new OrdersProducts
            {
                Order = order,
                Product = product,
                OrderId = order.OrderId,
                ProductId = product.ProductId
            };

            var orders = new List<Orders> { order };
            var products = new List<Products> { product };
            var ordersProducts = new List<OrdersProducts> { ordersProduct };

            var ordersDbSet = FakeDbSet.GetQueryableMockDbSet(orders);
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);
            var ordersProductsDbSet = FakeDbSet.GetQueryableMockDbSet(ordersProducts);

            context.Setup(svc => svc.Orders).Returns(ordersDbSet);
            context.Setup(svc => svc.Products).Returns(productsDbSet);
            context.Setup(svc => svc.OrdersProducts).Returns(ordersProductsDbSet);

            var repo = new OrdersLibraryRepository(mapperMock.Object);
            repo.SetContext(context.Object);

            // Act
            var result = await repo.UpdateAsync(orderToUpdate);

            //Assert
            mapperMock.Verify(svc => svc.Map(orderToUpdate, order), Times.Once);
        }

        [Theory, AutoMoqData]
        public async Task AddAsync_Successfully(Mock<LibraryContext> context, Mock<IMapper> mapperMock, List<Orders> orders, List<OrdersProducts>ordersProducts,
            OrderDTO orderDTO)
        {
            //Arrange
            var orderDbSet = FakeDbSet.GetQueryableMockDbSet(orders);
            var ordersProductsDbSet = FakeDbSet.GetQueryableMockDbSet(ordersProducts);
            context.Setup(svc => svc.OrdersProducts).Returns(ordersProductsDbSet);
            context.Setup(svc => svc.Orders).Returns(orderDbSet);

            mapperMock.Setup(svc => svc.Map<Orders>(orderDTO))
                .Returns(new Orders());

            var repo = new OrdersLibraryRepository(mapperMock.Object);
            repo.SetContext(context.Object);

            // Act
            await repo.AddAsync(orderDTO);

            //Assert
            context.Verify(x => x.Orders.AddAsync(It.IsAny<Orders>(), It.IsAny<CancellationToken>()), Times.Once);
            context.Verify(
                x => x.OrdersProducts.AddRangeAsync(It.IsAny<List<OrdersProducts>>(), It.IsAny<CancellationToken>()),
                Times.Once);
        }

        [Theory, AutoMoqData]
        public async Task DeleteAsync_Throws_ItemNotFoundException(OrdersLibraryRepository repo, Orders order, int id)
        {
            //Arrange
            var entityName = "Item";
            var orders = new List<Orders> { order };
            var ordersDbSet = FakeDbSet.GetQueryableMockDbSet(orders);

            var context = new Mock<LibraryContext>();
            context.Setup(svc => svc.Orders).Returns(ordersDbSet);
            repo.SetContext(context.Object);

            //Act
            Func<Task> act = async () => await repo.DeleteAsync(id);

            //Assert
            Exception result = await Assert.ThrowsAsync<ItemNotFoundException>(act);
            Assert.Equal(string.Format(ErrorMessageConsts.NotFound, entityName, id), result.Message);
        }

        [Theory, AutoMoqData]
        public async Task DeleteAsync_Successfully(OrdersLibraryRepository repo, Orders order, int id)
        {
            //Arrange
            order.OrderId = id;
            var orders = new List<Orders> { order };
            var ordersDbSet = FakeDbSet.GetQueryableMockDbSet(orders);

            var context = new Mock<LibraryContext>();
            context.Setup(svc => svc.Orders).Returns(ordersDbSet);
            repo.SetContext(context.Object);

            // Act
            await repo.DeleteAsync(id);

            //Assert
            context.Verify(x => x.Orders.Remove(order), Times.Once);
        }
    }
}