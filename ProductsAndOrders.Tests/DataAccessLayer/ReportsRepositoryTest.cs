using System.Collections.Generic;
using Moq;
using ProductsAndOrders.DataStorage;
using ProductsAndOrders.Models;
using ProductsAndOrders.Tests.DataAccessLayer.DbSetMocks;
using Xunit;

namespace ProductsAndOrders.Tests
{
    public class ReportsRepositoryTest
    {
        [Theory, AutoMoqData]
        public async void GetMostSaleableProductAsync_Successfully(Mock<LibraryContext> context, ReportsLibraryRepository repo,
             Products product)
        {
            //Arrange

            var products = new List<Products> { product };

            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);

            context.Setup(svc => svc.Products).Returns(productsDbSet);
            repo.SetContext(context.Object);

            // Act
            var result = await repo.GetMostSaleableProductAsync();

            //Assert
            Assert.Equal(result, product);
        }

        [Theory, AutoMoqData]
        public async void GetMostExpensiveOrderAsync_Successfully(ReportsLibraryRepository repo,
            Orders order, Products product)
        {
            //Arrange
            var ordersProduct = new OrdersProducts
            {
                Order = order,
                Product = product,
                OrderId = order.OrderId,
                ProductId = product.ProductId
            };

            var orders = new List<Orders> { order };
            var products = new List<Products> { product };
            var ordersProducts = new List<OrdersProducts> { ordersProduct };

            var ordersDbSet = FakeDbSet.GetQueryableMockDbSet(orders);
            var productsDbSet = FakeDbSet.GetQueryableMockDbSet(products);
            var ordersProductsDbSet = FakeDbSet.GetQueryableMockDbSet(ordersProducts);

            var context = new Mock<LibraryContext>();
            context.Setup(svc => svc.Orders).Returns(ordersDbSet);
            context.Setup(svc => svc.Products).Returns(productsDbSet);
            context.Setup(svc => svc.OrdersProducts).Returns(ordersProductsDbSet);
            repo.SetContext(context.Object);

            // Act
            var result = await repo.GetMostExpensiveOrderAsync();

            //Assert
            Assert.Equal(result.DestinationCity, order.DestinationCity);
            Assert.Equal(result.RecipientName, order.RecipientName);
        }
    }
}