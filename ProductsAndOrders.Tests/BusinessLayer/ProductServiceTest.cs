using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using AutoMapper;
using Moq;
using ProductsAndOrders.DataStorage;
using ProductsAndOrders.Models.BusinessModels;
using ProductsAndOrders.Models.DTOModels;
using ProductsAndOrders.Models.Exceptions;
using ProductsAndOrders.Services;
using Xunit;

namespace ProductsAndOrders.Tests.BusinessLayer
{
    public class ProductServiceTest
    {
        [Theory, AutoMoqData]
        public async Task AddProductAsync_Successfully([Frozen] Mock<IUnitOfWork> unitOfWorkMock, Products product,
            [Frozen] Mock<IMapper> mapper, BusinessProduct businessProduct, List<Products> products,
            [Frozen] Mock<IProductsLibraryRepository> usersRepositoryMock, ProductsService sut)
        {
            // Arrange
            usersRepositoryMock.Setup(svc => svc.GetAsync()).ReturnsAsync(products);
            mapper.Setup(svc => svc.Map<Products>(businessProduct)).Returns(product);

            // Act
            await sut.AddAsync(businessProduct);

            // Assert
            unitOfWorkMock.Verify(x => x.Commit(), Times.Once);
            unitOfWorkMock.Verify(x => x.ProductRepository.AddAsync(product), Times.Once);
        }

        [Theory, AutoMoqData]
        public async void AddProductAsync_Throws_BadRequest(BusinessProduct businessProduct, List<Products> products,
            [Frozen] Mock<IProductsLibraryRepository> usersRepositoryMock, ProductsService sut)
        {
            // Arrange
            businessProduct.Name = products.First().Name;
            usersRepositoryMock.Setup(svc => svc.GetAsync()).ReturnsAsync(products);

            //Act
            Func<Task> act = async () => await sut.AddAsync(businessProduct);

            //Assert
            Exception result = await Assert.ThrowsAsync<BadRequestException>(act);
            Assert.Equal("Product is alredy exist", result.Message);
        }

        [Theory, AutoMoqData]
        public async void UpdateProductAsync_Successfully([Frozen] Mock<IMapper> mapper, [Frozen] Mock<IUnitOfWork> unitOfWorkMock, Products updatedProduct,
            [Frozen] Mock<IProductsLibraryRepository> usersRepositoryMock, ProductsService sut,
            ProductToUpdate productToUpdate, BusinessProduct businessProduct)
        {
            // Arrange
            usersRepositoryMock.Setup(svc => svc.UpdateAsync(productToUpdate)).ReturnsAsync(updatedProduct);
            mapper.Setup(svc => svc.Map<ProductToUpdate>(businessProduct)).Returns(productToUpdate);
            mapper.Setup(svc => svc.Map<BusinessProduct>(updatedProduct)).Returns(businessProduct);

            // Act
            var result = await sut.UpdateAsync(businessProduct);

            // Assert
            unitOfWorkMock.Verify(x => x.Commit(), Times.Once);
            Assert.Equal(result, businessProduct);
        }

        [Theory, AutoMoqData]
        public async void DeleteProductAsync_Successfully(
            [Frozen] Mock<IUnitOfWork> unitOfWorkMock, int id, ProductsService sut)
        {
            // Arrange

            // Act
            await sut.DeleteAsync(id);

            // Assert
            unitOfWorkMock.Verify(x => x.Commit(), Times.Once);
            unitOfWorkMock.Verify(x => x.ProductRepository.DeleteAsync(id), Times.Once);
        }

        [Theory, AutoMoqData]
        public async void GetByQueryAsync_ReturnsAllProducts([Frozen] Mock<IMapper> mapper, List<Products> products, List<BusinessProduct> businessProducts,
            [Frozen] Mock<IProductsLibraryRepository> productsRepositoryMock, ProductsService sut)
        {
            // Arrange
            var query = new BusinessSearchProduct();
            productsRepositoryMock.Setup(svc => svc.GetAsync()).ReturnsAsync(products);
            mapper.Setup(x => x.Map<List<BusinessProduct>>(products)).Returns(businessProducts);

            // Act
            var result = await sut.GetListAsync(query);

            // Assert
            Assert.Equal(3, result.Count);
        }

        [Theory, AutoMoqData]
        public async void GetByQueryAsync_ReturnsOneProduct([Frozen] Mock<IMapper> mapper, List<Products> products, List<BusinessProduct> businessProducts,
            [Frozen] Mock<IProductsLibraryRepository> productsRepositoryMock, ProductsService sut)
        {
            // Arrange
            var query = new BusinessSearchProduct { Name = "Mac" };
            businessProducts.First().Name = query.Name;
            productsRepositoryMock.Setup(svc => svc.GetAsync()).ReturnsAsync(products);
            mapper.Setup(x => x.Map<List<BusinessProduct>>(products)).Returns(businessProducts);

            // Act
            var result = await sut.GetListAsync(query);

            // Assert
            Assert.Single(result);
            Assert.Equal(query.Name, result.First().Name);
        }

        [Theory, AutoMoqData]
        public async void GetByQueryAsync_Throws_ItemNotFoundException([Frozen] Mock<IMapper> mapper, List<Products> products, List<BusinessProduct> businessProducts,
            [Frozen] Mock<IProductsLibraryRepository> productsRepositoryMock, ProductsService sut)
        {
            // Arrange
            var query = new BusinessSearchProduct { Name = "Mac" };
            productsRepositoryMock.Setup(svc => svc.GetAsync()).ReturnsAsync(products);
            mapper.Setup(x => x.Map<List<BusinessProduct>>(products)).Returns(businessProducts);

            //Act
            Func<Task> act = async () => await sut.GetListAsync(query);

            //Assert
            Exception result = await Assert.ThrowsAsync<ItemNotFoundException>(act);
            Assert.Equal("Products not found!", result.Message);
        }

        [Theory, AutoMoqData]
        public async void GetByIdAsync_Successfully(Products product, [Frozen] Mock<IMapper> mapper,
            int id, BusinessProduct businessProduct,
            [Frozen] Mock<IProductsLibraryRepository> productsRepositoryMock, ProductsService sut)
        {
            // Arrange
            productsRepositoryMock.Setup(svc => svc.GetAsync(id)).ReturnsAsync(product);
            mapper.Setup(x => x.Map<BusinessProduct>(product)).Returns(businessProduct);

            // Act
            var result = await sut.GetAsync(id);

            // Assert
            Assert.Equal(businessProduct.Name, result.Name);
        }

        [Theory, AutoMoqData]
        public async void GetListAsync_Successfully(List<Products> products, [Frozen] Mock<IMapper> mapper,
            List<BusinessProduct> businessProducts,
            [Frozen] Mock<IProductsLibraryRepository> productsRepositoryMock, ProductsService sut)
        {
            // Arrange
            productsRepositoryMock.Setup(svc => svc.GetAsync()).ReturnsAsync(products);
            mapper.Setup(x => x.Map<List<BusinessProduct>>(products)).Returns(businessProducts);

            // Act
            var result = await sut.GetListAsync();

            // Assert
            Assert.Equal(businessProducts.Count, result.Count());
        }
    }

}
