using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture.Xunit2;
using AutoMapper;
using Moq;
using ProductsAndOrders.DataStorage;
using ProductsAndOrders.Models.BusinessModels;
using ProductsAndOrders.Models.DTOModels;
using ProductsAndOrders.Models.Exceptions;
using ProductsAndOrders.Services;
using Xunit;

namespace ProductsAndOrders.Tests.BusinessLayer
{
    public class OrderServiceTest
    {
        [Theory, AutoMoqData]
        public async Task AddOrderAsync_Successfully([Frozen]Mock<IUnitOfWork> unitOfWorkMock,
            [Frozen]Mock<IMapper> mapper, BusinessOrder order, [Frozen]Mock<IProductsLibraryRepository> productsRepositoryMock,
            OrdersService sut, OrderDTO orderDto)
        {
            // Arrange
            var name = "Mac";
            var businessProducts = new List<BusinessProduct> { new BusinessProduct { Name = name } };
            var products = new List<Products> { new Products { Name = name } };
            order.Products = businessProducts;
            productsRepositoryMock.Setup(svc => svc.GetAsync()).ReturnsAsync(products);
            mapper.Setup(svc => svc.Map<OrderDTO>(order)).Returns(orderDto);

            // Act
            await sut.AddAsync(order);

            // Assert
            unitOfWorkMock.Verify(x => x.Commit(), Times.Once);
            unitOfWorkMock.Verify(x => x.OrderRepository.AddAsync(orderDto), Times.Once);
        }

        [Theory, AutoMoqData]
        public async Task AddOrderAsync_Throws_BadRequestException([Frozen]Mock<IUnitOfWork> unitOfWorkMock,
            BusinessOrder order, [Frozen]Mock<IProductsLibraryRepository> productsRepositoryMock,
            OrdersService sut, List<BusinessProduct> businessProducts, List<Products> products)
        {
            // Arrange;
            order.Products = businessProducts;
            productsRepositoryMock.Setup(svc => svc.GetAsync()).ReturnsAsync(products);

            //Act
            Func<Task> act = async () => await sut.AddAsync(order);

            //Assert
            Exception result = await Assert.ThrowsAsync<BadRequestException>(act);
            Assert.Equal("Not all products exist", result.Message);
        }

        [Theory, AutoMoqData]
        public async Task UpdateOrderAsync_Successfully([Frozen]Mock<IUnitOfWork> unitOfWorkMock,
            BusinessOrder businessOrder, [Frozen]Mock<IMapper> mapper, OrderToUpdate orderToUpdate,
            [Frozen]Mock<IOrdersLibraryRepository> orderRepositoryMock, OrdersService sut,
            OrderDTO updatedOrder)
        {
            // Arrange
            orderRepositoryMock.Setup(svc => svc.UpdateAsync(orderToUpdate)).ReturnsAsync(updatedOrder);
            mapper.Setup(svc => svc.Map<OrderToUpdate>(businessOrder)).Returns(orderToUpdate);
            mapper.Setup(svc => svc.Map<BusinessOrder>(updatedOrder)).Returns(businessOrder);

            // Act
            var result = await sut.UpdateAsync(businessOrder);

            // Assert
            unitOfWorkMock.Verify(x => x.Commit(), Times.Once);
            Assert.Equal(result, businessOrder);
        }

        [Theory, AutoMoqData]
        public async Task DeleteOrderAsync_Successfully([Frozen]Mock<IUnitOfWork> unitOfWorkMock, int id,
            OrdersService sut)
        {
            // Arrange

            // Act
            await sut.DeleteAsync(id);

            // Assert
            unitOfWorkMock.Verify(x => x.Commit(), Times.Once);
            unitOfWorkMock.Verify(x => x.OrderRepository.DeleteAsync(id), Times.Once);
        }

        [Theory, AutoMoqData]
        public async Task GetByQueryAsync_ReturnsAllOrders([Frozen]Mock<IMapper> mapper,
            [Frozen]Mock<IOrdersLibraryRepository> orderRepositoryMock, OrdersService sut,
            List<OrderDTO> orders, List<BusinessOrder> businessOrders)
        {
            // Arrange
            var query = new BusinessSearchOrder();
            orderRepositoryMock.Setup(svc => svc.GetAsync()).ReturnsAsync(orders);
            mapper.Setup(x => x.Map<List<BusinessOrder>>(orders)).Returns(businessOrders);

            // Act
            var result = await sut.GetListAsync(query);

            // Assert
            Assert.Equal(businessOrders.Count, result.Count);
        }

        [Theory, AutoMoqData]
        public async Task GetByQueryAsync_ReturnsRightOrders([Frozen]Mock<IMapper> mapper, List<BusinessOrder> businessOrders, List<OrderDTO> orders,
            [Frozen]Mock<IOrdersLibraryRepository> orderRepositoryMock, OrdersService sut)
        {
            // Arrange
            var query = new BusinessSearchOrder { RecipientName = "Vlad" };
            businessOrders.First().RecipientName = query.RecipientName;
            orderRepositoryMock.Setup(svc => svc.GetAsync()).ReturnsAsync(orders);
            mapper.Setup(x => x.Map<List<BusinessOrder>>(orders)).Returns(businessOrders);

            // Act
            var result = await sut.GetListAsync(query);

            // Assert
            Assert.Single(result);
            Assert.Equal(result.First().RecipientName, query.RecipientName);
        }

        [Theory, AutoMoqData]
        public async Task GetByIdAsync_Successfully(OrderDTO order, [Frozen]Mock<IMapper> mapper,
            int id, BusinessOrder businessOrder,
            [Frozen]Mock<IOrdersLibraryRepository> orderRepositoryMock, OrdersService sut)
        {
            // Arrange
            orderRepositoryMock.Setup(svc => svc.GetAsync(id)).ReturnsAsync(order);
            mapper.Setup(x => x.Map<BusinessOrder>(order)).Returns(businessOrder);

            // Act
            var result = await sut.GetAsync(id);

            // Assert
            Assert.Equal(businessOrder.RecipientName, businessOrder.RecipientName);
        }

        [Theory, AutoMoqData]
        public async void GetAllProductsAsync_Successfully(List<OrderDTO> orders, [Frozen]Mock<IMapper> mapper,
            List<BusinessOrder> businessOrders,
            [Frozen]Mock<IOrdersLibraryRepository> orderRepositoryMock, OrdersService sut)
        {
            // Arrange
            orderRepositoryMock.Setup(svc => svc.GetAsync()).ReturnsAsync(orders);
            mapper.Setup(x => x.Map<List<BusinessOrder>>(orders)).Returns(businessOrders);

            // Act
            var result = await sut.GetListAsync();

            // Assert
            Assert.Equal(businessOrders.Count, result.Count());
        }

    }
}