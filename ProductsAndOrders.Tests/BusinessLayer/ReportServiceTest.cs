using System.Collections.Generic;
using AutoFixture.Xunit2;
using AutoMapper;
using Moq;
using ProductsAndOrders.DataStorage;
using ProductsAndOrders.Models;
using ProductsAndOrders.Models.BusinessModels;
using ProductsAndOrders.Models.DTOModels;
using ProductsAndOrders.Services;
using Xunit;

namespace ProductsAndOrders.Tests.BusinessLayer
{
    public class ReportServiceTest
    {
        [Theory, AutoMoqData]
        public async void GetMostSaleableProductAsync(Products product, [Frozen]Mock<IMapper> mapperMock,
            BusinessProduct businessProduct,
            [Frozen]Mock<IReportsLibraryRepository> reportsRepositoryMock, ReportsService sut)
        {
            // Arrange
            reportsRepositoryMock.Setup(svc => svc.GetMostSaleableProductAsync()).ReturnsAsync(product);
            mapperMock.Setup(x => x.Map<BusinessProduct>(product)).Returns(businessProduct);

            // Act
            var result = await sut.GetMostSaleableProductAsync();

            // Assert
            Assert.Equal(businessProduct.Name, result.Name);
        }

        [Theory, AutoMoqData]
        public async void GetMostExpensiveOrderAsync(OrderDTO order, [Frozen]Mock<IMapper> mapperMock,
            BusinessOrder businessOrder,
            [Frozen]Mock<IReportsLibraryRepository> reportsRepositoryMock, ReportsService sut)
        {
            // Arrange
            reportsRepositoryMock.Setup(svc => svc.GetMostExpensiveOrderAsync()).ReturnsAsync(order);
            mapperMock.Setup(x => x.Map<BusinessOrder>(order)).Returns(businessOrder);

            // Act
            var result = await sut.GetMostExpensiveOrderAsync();

            // Assert
            Assert.Equal(businessOrder.RecipientName, result.RecipientName);
        }
    }
}