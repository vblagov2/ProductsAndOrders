using System.Threading.Tasks;
using ProductsAndOrders.Models;
using ProductsAndOrders.Models.BusinessModels;

namespace ProductsAndOrders.Services
{
    public interface IReportsService
    {
        Task<BusinessProduct> GetMostSaleableProductAsync();
        Task<BusinessOrder> GetMostExpensiveOrderAsync();
    }
}