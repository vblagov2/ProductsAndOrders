using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ProductsAndOrders.DataStorage;
using ProductsAndOrders.Models.BusinessModels;
using ProductsAndOrders.Models.DTOModels;
using ProductsAndOrders.Models.Exceptions;

namespace ProductsAndOrders.Services
{
    public class ProductsService : IProductsService
    {
        #region Fields

        private readonly IUnitOfWork _repository;
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        public ProductsService(IUnitOfWork repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        #endregion

        #region Methods

        public async Task<IEnumerable<BusinessProduct>> GetListAsync()
        {
            var products = await _repository.ProductRepository.GetAsync();
            return _mapper.Map<List<BusinessProduct>>(products);
        }

        public async Task<BusinessProduct> GetAsync(int id)
        {
            var product = await _repository.ProductRepository.GetAsync(id);
            return _mapper.Map<BusinessProduct>(product);
        }

        public async Task DeleteAsync(int id)
        {
            await _repository.ProductRepository.DeleteAsync(id);
            _repository.Commit();
        }

        public async Task<List<BusinessProduct>> GetListAsync(BusinessSearchProduct query)
        {
            var products = await GetListAsync();
            if (String.IsNullOrEmpty(query.Name))
            {
                return products.ToList();
            }

            var result = products.FirstOrDefault(item => item.Name == query.Name);
            if (result is null)
            {
                throw new ItemNotFoundException("Products not found!");
            }

            return new List<BusinessProduct> { result };
        }

        public async Task<BusinessProduct> UpdateAsync(BusinessProduct product)
        {
            var productToUpdate = _mapper.Map<ProductToUpdate>(product);
            var updatedProduct = await _repository.ProductRepository.UpdateAsync(productToUpdate);
            _repository.Commit();
            var updatedBusinesProduct = _mapper.Map<BusinessProduct>(updatedProduct);

            return updatedBusinesProduct;
        }

        public async Task AddAsync(BusinessProduct product)
        {
            var products = await _repository.ProductRepository.GetAsync();
            var prod = products.FirstOrDefault(item => item.Name == product.Name);
            if (!(prod is null))
            {
                throw new BadRequestException("Product is alredy exist");
            }

            var newProduct = _mapper.Map<Products>(product);
            await _repository.ProductRepository.AddAsync(newProduct);
            _repository.Commit();
        }

        #endregion
    }
}