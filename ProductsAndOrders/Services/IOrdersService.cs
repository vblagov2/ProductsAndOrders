using System.Collections.Generic;
using System.Threading.Tasks;
using ProductsAndOrders.Models.BusinessModels;

namespace ProductsAndOrders.Services
{
    public interface IOrdersService
    {
        Task<List<BusinessOrder>> GetListAsync(BusinessSearchOrder query);
        Task<BusinessOrder> UpdateAsync(BusinessOrder order);
        Task AddAsync(BusinessOrder order);
        Task<IEnumerable<BusinessOrder>> GetListAsync();
        Task<BusinessOrder> GetAsync(int id);
        Task DeleteAsync(int id);
    }
}