using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Castle.Core.Internal;
using ProductsAndOrders.DataStorage;
using ProductsAndOrders.Models.BusinessModels;
using ProductsAndOrders.Models.DTOModels;
using ProductsAndOrders.Models.Exceptions;

namespace ProductsAndOrders.Services
{
    public class OrdersService : IOrdersService
    {
        #region Fields

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        public OrdersService(IUnitOfWork repository, IMapper mapper)
        {
            _unitOfWork = repository;
            _mapper = mapper;
        }

        #endregion

        #region Methods

        public async Task<IEnumerable<BusinessOrder>> GetListAsync()
        {
            var orders = await _unitOfWork.OrderRepository.GetAsync();
            var businessOrders = _mapper.Map<List<BusinessOrder>>(orders);

            return businessOrders;
        }

        public async Task<BusinessOrder> GetAsync(int id)
        {
            var order = await _unitOfWork.OrderRepository.GetAsync(id);
            var resultOrder = _mapper.Map<BusinessOrder>(order);

            return resultOrder;
        }

        public async Task DeleteAsync(int id)
        {
            await _unitOfWork.OrderRepository.DeleteAsync(id);
            _unitOfWork.Commit();
        }

        public async Task<List<BusinessOrder>> GetListAsync(BusinessSearchOrder query)
        {
            var orders = await GetListAsync();
            if (String.IsNullOrEmpty(query.RecipientName))
            {
                return orders.ToList();
            }

            var resultOrders = orders.Where(item => item.RecipientName == query.RecipientName).ToList();
            if (resultOrders.IsNullOrEmpty())
            {
                throw new ItemNotFoundException("Orders not found!");
            }

            return resultOrders;
        }

        public async Task<BusinessOrder> UpdateAsync(BusinessOrder order)
        {
            var orderToUpdate = _mapper.Map<OrderToUpdate>(order);
            var updatedOrder = await _unitOfWork.OrderRepository.UpdateAsync(orderToUpdate);
            _unitOfWork.Commit();
            var result = _mapper.Map<BusinessOrder>(updatedOrder);
             
            return result;
        }

        public async Task AddAsync(BusinessOrder order)
        {
            var existedProducts = await GetProducts(order.Products);
            if (existedProducts.Count != order.Products.Count)
            {
                throw new BadRequestException("Not all products exist");
            }

            var orderToAdd = _mapper.Map<OrderDTO>(order);
            orderToAdd.Products = existedProducts;

            await _unitOfWork.OrderRepository.AddAsync(orderToAdd);
            _unitOfWork.Commit();
        }

        private async Task<List<Products>> GetProducts(List<BusinessProduct> products)
        {
            var productsNames = products.Select(item => item.Name).ToList();
            var productsDb = await _unitOfWork.ProductRepository.GetAsync();
            var existedProducts = productsDb.Where(item => productsNames.Contains(item.Name)).ToList();

            return existedProducts;
        }

        #endregion
    }
}