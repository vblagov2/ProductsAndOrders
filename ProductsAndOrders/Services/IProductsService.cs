using System.Collections.Generic;
using System.Threading.Tasks;
using ProductsAndOrders.Models.BusinessModels;

namespace ProductsAndOrders.Services
{
    public interface IProductsService
    {
        Task<List<BusinessProduct>> GetListAsync(BusinessSearchProduct query);
        Task<BusinessProduct> UpdateAsync(BusinessProduct product);
        Task AddAsync(BusinessProduct product);
        Task<IEnumerable<BusinessProduct>> GetListAsync();
        Task<BusinessProduct> GetAsync(int id);
        Task DeleteAsync(int id);
    }
}