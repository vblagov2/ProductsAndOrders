using System.Threading.Tasks;
using AutoMapper;
using ProductsAndOrders.DataStorage;
using ProductsAndOrders.Models.BusinessModels;

namespace ProductsAndOrders.Services
{
    public class ReportsService : IReportsService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ReportsService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<BusinessProduct> GetMostSaleableProductAsync()
        {
            var product = await _unitOfWork.ReportsRepository.GetMostSaleableProductAsync();

            var result = _mapper.Map<BusinessProduct>(product);
            return result;
        }

        public async Task<BusinessOrder> GetMostExpensiveOrderAsync()
        {
            var order = await _unitOfWork.ReportsRepository.GetMostExpensiveOrderAsync();

            var result = _mapper.Map<BusinessOrder>(order);
            return result;
        }
    }
}