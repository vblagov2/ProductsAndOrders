﻿
namespace ProductsAndOrders.Exceptions
{
    public class ErrorMessageConsts
    {
        public const string NotFound = "Could not find {0} by the key(s) '{1}'";
    }
}
