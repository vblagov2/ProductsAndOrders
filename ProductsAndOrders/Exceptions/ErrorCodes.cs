﻿
namespace ProductsAndOrders.Exceptions
{
    public static class ErrorCodes
    {
        public const string Unknown = "Unknown";

        public const string NotFound = "NotFound";

        public const string BadRequest = "BadRequest";
    }
}
