using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProductsAndOrders.Models;
using ProductsAndOrders.Models.BusinessModels;
using ProductsAndOrders.Models.PresentationModels;
using ProductsAndOrders.Models.Requests;
using ProductsAndOrders.Services;

namespace ProductsAndOrders.Controllers
{
    [Route("api/orders")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        #region Fields

        private readonly IOrdersService _service;
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        public OrdersController(IOrdersService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        #endregion

        #region HTTPMethods

        //GET api/orders?name=vlad"
        [HttpGet]
        public async Task<IActionResult> GetAsync([FromQuery] SearchOrderQuery query)
        {
            var searchOrder = _mapper.Map<BusinessSearchOrder>(query);
            var orders = await _service.GetListAsync(searchOrder);

            var result = _mapper.Map<List<PresentationOrder>>(orders);

            return Ok(result);
        }

        //GET api/orders/1
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsyncById(int id)
        {
            var order = await _service.GetAsync(id);
            var result = _mapper.Map<PresentationOrder>(order);

            return Ok(result);
        }

        //PUT api/orders/1
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateAsync(int id, [FromBody] UpdateOrderRequest request)
        {
            var order = _mapper.Map<BusinessOrder>(request);
            order.OrderId = id;
            var updatedOrder = await _service.UpdateAsync(order);
            var result = _mapper.Map<PresentationOrder>(updatedOrder);

            return Ok(result);
        }

        //POST api/orders
        [HttpPost]
        public async Task<IActionResult> AddAsync([FromBody] AddOrderRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var order = _mapper.Map<BusinessOrder>(request);
            await _service.AddAsync(order);

            return Ok();
        }

        //DELETE api/orders/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await _service.DeleteAsync(id);

            return Ok();
        }

        #endregion
    }
}