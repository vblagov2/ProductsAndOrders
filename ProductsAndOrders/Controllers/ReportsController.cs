using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProductsAndOrders.Models.PresentationModels;
using ProductsAndOrders.Services;

namespace ProductsAndOrders.Controllers
{
    [Route("api/reports")]
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    public class ReportsController : ControllerBase
    {
        private readonly IReportsService _service;
        private readonly IMapper _mapper;

        public ReportsController(IReportsService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        //GET api/reports/mostSaleableProduct"
        [HttpGet("most-saleable-product")]
        public async Task<IActionResult> GetMostSaleableProductAsync()
        {
            var result = await _service.GetMostSaleableProductAsync();

            var presentationResult = _mapper.Map<PresentationProduct>(result);

            return new OkObjectResult(presentationResult);
        }

        //GET api/reports/mostSaleableProduct"
        [HttpGet("most-expensive-order")]
        public async Task<IActionResult> GetMostExpensiveOrderAsync()
        {
            var result = await _service.GetMostExpensiveOrderAsync();
            
            var presentationResult = _mapper.Map<PresentationOrder>(result);

            return new OkObjectResult(presentationResult);
        }
    }
}