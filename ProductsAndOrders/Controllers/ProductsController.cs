using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProductsAndOrders.Models;
using ProductsAndOrders.Models.BusinessModels;
using ProductsAndOrders.Models.PresentationModels;
using ProductsAndOrders.Models.Requests;
using ProductsAndOrders.Services;

namespace ProductsAndOrders.Controllers
{
    [Route("api/products")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        #region Fields

        private readonly IProductsService _service;
        private readonly IMapper _mapper;

        #endregion

        #region Constructors

        public ProductsController(IProductsService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }

        #endregion

        #region HTTPMethods

        // GET:
        [HttpGet]
        public async Task<IActionResult> GetAsync([FromQuery] SearchProductQuery query)
        {
            var searchProduct = _mapper.Map<BusinessSearchProduct>(query);
            var product = await _service.GetListAsync(searchProduct);

            var result = _mapper.Map<List<PresentationProduct>>(product);

            return Ok(result);
        }

        //GET api/products/1
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsyncById(int id)
        {
            var product = await _service.GetAsync(id);

            var result = _mapper.Map<PresentationProduct>(product);

            return Ok(result);
        }

        //PUT api/products/1
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> UpdateAsync(int id, [FromBody] UpdateProductRequest request)
        {
            var product = _mapper.Map<BusinessProduct>(request);
            product.ProductId = id;
            var updatedProduct = await _service.UpdateAsync(product);
            var result = _mapper.Map<PresentationProduct>(updatedProduct);

            return Ok(result);
        }

        //POST api/products
        [HttpPost]
        [Authorize(AuthenticationSchemes = "Bearer")]
        public async Task<IActionResult> AddAsync([FromBody] AddProductRequest request)
        {
            var product = _mapper.Map<BusinessProduct>(request);
            await _service.AddAsync(product);

            return Ok();
        }

        //DELETE api/products/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(int id)
        {
            await _service.DeleteAsync(id);

            return Ok();
        }

        #endregion
    }
}