﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using ProductsAndOrders.Exceptions;
using ProductsAndOrders.Models.Exceptions;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace ProductsAndOrders.MiddleWares
{
    public class ExceptionsMiddleware
    {
        private readonly Dictionary<string, HttpStatusCode> ErrorCodesMap = new Dictionary<string, HttpStatusCode>
        {
            { ErrorCodes.NotFound, HttpStatusCode.NotFound },
            { ErrorCodes.BadRequest, HttpStatusCode.BadRequest }
        };

        private readonly RequestDelegate next;

        public ExceptionsMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            string error = exception.Message;
            string errorCode = ErrorCodes.Unknown;
            string stackTrace = exception.StackTrace;
            var httpStatusCode = HttpStatusCode.InternalServerError;

            if (exception is BaseException baseException)
            {
                errorCode = baseException.ErrorCode;
                httpStatusCode = ErrorCodesMap[errorCode];
                stackTrace = null;
            }
            var errorModel = new ErrorModel()
            {
                Error = error,
                ErrorCode = errorCode,
                StackTrace = stackTrace
            };

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)httpStatusCode;
            return context.Response.WriteAsync(JsonConvert.SerializeObject(errorModel));
        }
    }
}
