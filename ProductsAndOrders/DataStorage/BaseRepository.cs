using Microsoft.EntityFrameworkCore;
using ProductsAndOrders.Exceptions;
using ProductsAndOrders.Models.Exceptions;

namespace ProductsAndOrders.DataStorage
{
    public abstract class BaseRepository<TContext> where TContext : DbContext
    {
        protected virtual string EntityName { get => "Item"; }

        protected void ValidateIsFound(object key, object item, string entityName = null)
        {
            if (item == null)
            {
                throw new ItemNotFoundException(string.Format(ErrorMessageConsts.NotFound, entityName ?? EntityName, key));
            }
        }

        protected void ValidateIsFound(object item)
        {
            if (item == null)
            {
                throw new ItemNotFoundException("Item not found!");
            }
        }

        public abstract void SetContext(TContext context);
    }
}