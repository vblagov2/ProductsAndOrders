using ProductsAndOrders.Models.DTOModels;
using System.Threading.Tasks;

namespace ProductsAndOrders.DataStorage
{
    public interface IOrdersLibraryRepository : ILibraryRepository<OrderDTO>
    {
        Task<OrderDTO> UpdateAsync(OrderToUpdate order);
        Task AddAsync(OrderDTO order);
    }
}