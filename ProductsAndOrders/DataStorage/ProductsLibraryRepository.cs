using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProductsAndOrders.Models.DTOModels;

namespace ProductsAndOrders.DataStorage
{
    public class ProductsLibraryRepository : BaseRepository<LibraryContext>, IProductsLibraryRepository
    {
        #region Fields

        private LibraryContext _libraryContext;
        private readonly IMapper _mapper;

        #endregion

        public ProductsLibraryRepository(IMapper mapper)
        {
            _mapper = mapper;
        }

        #region Methods

        public async Task<IEnumerable<Products>> GetAsync()
        {
            var products = await _libraryContext.Products.Where(prod => !prod.IsDeleted.Value).ToListAsync();
            return products;
        }

        public async Task<Products> GetAsync(int id)
        {
            var product = await _libraryContext.Products.FirstOrDefaultAsync(
                prod => prod.ProductId == id && !prod.IsDeleted.Value);
            ValidateIsFound(id, product);

            return product;
        }

        public async Task<Products> UpdateAsync(ProductToUpdate productToUpdate)
        {
            var product = await GetAsync(productToUpdate.ProductId);
            return _mapper.Map(productToUpdate, product);
        }

        public async Task AddAsync(Products product)
        {
            await _libraryContext.Products.AddAsync(product);
        }

        public async Task DeleteAsync(int id)
        {
            var product = await GetAsync(id);
            ValidateIsFound(id, product);
            _libraryContext.Products.Remove(product);
        }

        #endregion

        public override void SetContext(LibraryContext context)
        {
            _libraryContext = context;
        }
    }
}