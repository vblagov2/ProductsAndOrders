using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductsAndOrders.DataStorage
{
    public interface ILibraryRepository<T>
    {
        Task<IEnumerable<T>> GetAsync();
        Task<T> GetAsync(int id);
        Task DeleteAsync(int id);
    }
}