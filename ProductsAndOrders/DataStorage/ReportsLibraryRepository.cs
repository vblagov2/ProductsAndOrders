using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProductsAndOrders.Models.DTOModels;

namespace ProductsAndOrders.DataStorage
{
    public class ReportsLibraryRepository : BaseRepository<LibraryContext>, IReportsLibraryRepository
    {
        private LibraryContext _libraryContext;

        public async Task<Products> GetMostSaleableProductAsync()
        {
            var product = await _libraryContext.Products
                    .OrderByDescending(item => item.OrdersProducts.Count)
                    .FirstOrDefaultAsync();

            ValidateIsFound(product);
            return product;
        }

        public async Task<OrderDTO> GetMostExpensiveOrderAsync()
        {
            var order = await _libraryContext.OrdersProducts.GroupBy(item => item.Order)
                .OrderByDescending(op => op.Sum(item => item.Price))
                .Select(item => new OrderDTO
                {
                    OrderId = item.Key.OrderId,
                    RecipientName = item.Key.RecipientName,
                    DestinationCity = item.Key.DestinationCity,
                    Products = item.Select(op => op.Product).ToList()
                }).FirstOrDefaultAsync();

            ValidateIsFound(order);
            return order;
        }

        public override void SetContext(LibraryContext context)
        {
            _libraryContext = context;
        }
    }
}