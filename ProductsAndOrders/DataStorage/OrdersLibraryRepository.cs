using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProductsAndOrders.Models;
using ProductsAndOrders.Models.DTOModels;

namespace ProductsAndOrders.DataStorage
{
    public class OrdersLibraryRepository : BaseRepository<LibraryContext>, IOrdersLibraryRepository
    {
        #region Fields

        private LibraryContext _libraryContext;
        private readonly IMapper _mapper;

        #endregion

        public OrdersLibraryRepository(IMapper mapper)
        {
            _mapper = mapper;
        }

        #region Methods

        public async Task<IEnumerable<OrderDTO>> GetAsync()
        {
            var orders = await _libraryContext.OrdersProducts.GroupBy(item => item.Order)
                .Select(item => new OrderDTO
                {
                    OrderId = item.Key.OrderId,
                    RecipientName = item.Key.RecipientName,
                    DestinationCity = item.Key.DestinationCity,
                    Products = item.Select(op => op.Product).ToList()
                }).ToListAsync();


            return orders;
        }

        public async Task<OrderDTO> GetAsync(int id)
        {
            var resultOrders = await GetAsync();
            var order = resultOrders.FirstOrDefault(ord => ord.OrderId == id);
            ValidateIsFound(id, order);
            return order;
        }

        public async Task DeleteAsync(int id)
        {
            var order = await _libraryContext.Orders.FirstOrDefaultAsync(ord => ord.OrderId == id);
            ValidateIsFound(id, order);
            _libraryContext.Orders.Remove(order);
        }

        public async Task<OrderDTO> UpdateAsync(OrderToUpdate order)
        {
            var updatedOrder = await _libraryContext.Orders.FirstOrDefaultAsync(item => item.OrderId == order.OrderId);
            ValidateIsFound(updatedOrder);
            _mapper.Map(order, updatedOrder);
            return await GetAsync(updatedOrder.OrderId);
        }

        public async Task AddAsync(OrderDTO order)
        {
            var ordersProducts = new List<OrdersProducts>();
            var addOrder = _mapper.Map<Orders>(order);
            foreach (var product in order.Products)
            {
                ordersProducts.Add(new OrdersProducts
                {
                    Order = addOrder,
                    Product = product,
                    Price = product.Price
                });
            }

            foreach (var item in ordersProducts)
            {
                addOrder.OrdersProducts.Add(item);
            }

            await _libraryContext.Orders.AddAsync(addOrder);
            await _libraryContext.OrdersProducts.AddRangeAsync(ordersProducts);
        }

        public override void SetContext(LibraryContext context)
        {
            _libraryContext = context;
        }

        #endregion
    }
}