using System.Threading.Tasks;
using ProductsAndOrders.Models.DTOModels;

namespace ProductsAndOrders.DataStorage
{
    public interface IReportsLibraryRepository
    {
        Task<Products> GetMostSaleableProductAsync();
        Task<OrderDTO> GetMostExpensiveOrderAsync();
    }
}