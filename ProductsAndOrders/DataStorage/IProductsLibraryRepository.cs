using ProductsAndOrders.Models.DTOModels;
using System.Threading.Tasks;

namespace ProductsAndOrders.DataStorage
{
    public interface IProductsLibraryRepository : ILibraryRepository<Products>
    {
        Task<Products> UpdateAsync(ProductToUpdate product);
        Task AddAsync(Products product);
    }
}