using System;

namespace ProductsAndOrders.DataStorage
{
    public interface IUnitOfWork : IDisposable
    {
        IProductsLibraryRepository ProductRepository { get; }
        IOrdersLibraryRepository OrderRepository { get; }
        IReportsLibraryRepository ReportsRepository { get; }
        void Commit();
    }
}