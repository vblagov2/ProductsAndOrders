using System;
using Microsoft.EntityFrameworkCore;

namespace ProductsAndOrders.DataStorage
{
    public class UnitOfWork<TContext> : IUnitOfWork
        where TContext : DbContext
    {
        private readonly TContext context;
        private readonly IServiceProvider serviceProvider;

        #region Repositories

        public IProductsLibraryRepository ProductRepository =>
            ResolveRepository<IProductsLibraryRepository>();

        public IOrdersLibraryRepository OrderRepository =>
            ResolveRepository<IOrdersLibraryRepository>();

        public IReportsLibraryRepository ReportsRepository =>
            ResolveRepository<IReportsLibraryRepository>();

        #endregion

        public UnitOfWork(TContext context, IServiceProvider serviceProvider)
        {
            this.context = context;
            this.serviceProvider = serviceProvider;
        }

        public void Commit()
        {
            context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }

        private T ResolveRepository<T>()
        {
            var key = typeof(T);
            var repository = (T)serviceProvider.GetService(key);
            if (repository == null)
            {
                throw new ApplicationException($"Requested repository with the type '{key.Name}' is not registered.");
            }

            var baseRepository = repository as BaseRepository<TContext>;
            baseRepository.SetContext(context);
            return repository;
        }
    }
}