using AutoMapper;
using ProductsAndOrders.Models;
using ProductsAndOrders.Models.BusinessModels;
using ProductsAndOrders.Models.DTOModels;
using ProductsAndOrders.Models.PresentationModels;
using ProductsAndOrders.Models.Requests;
using System.Collections.Generic;

namespace ProductsAndOrders.Tools
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Business and data logic layer
            CreateMap<Products, BusinessProduct>();

            CreateMap<BusinessProduct, Products>()
                .BeforeMap((s, d) => d.IsDeleted = false)
                .ForMember(prod => prod.Name,
                    opt => opt.MapFrom(c => c.Name))
                .ForMember(prod => prod.Price,
                    opt => opt.MapFrom(src => src.Price))
                .ForMember(prod => prod.ProductId,
                    opt => opt.MapFrom(src => src.ProductId));


            CreateMap<Orders, BusinessOrder>();

            CreateMap<BusinessOrder, Orders>();

            // Requests and presentation layer

            CreateMap<AddOrderRequest, PresentationOrder>();

            CreateMap<UpdateOrderRequest, PresentationOrder>();

            CreateMap<AddProductRequest, PresentationProduct>();

            CreateMap<UpdateProductRequest, PresentationProduct>();

            // Business and presentation layer

            CreateMap<PresentationProduct, BusinessProduct>();

            CreateMap<BusinessProduct, PresentationProduct>();
            CreateMap<BusinessOrder, PresentationOrder>();

            CreateMap<PresentationOrder, BusinessOrder>();

            // Business and dataAccess layer
            //Orders
            CreateMap<OrderDTO, BusinessOrder>();
            CreateMap<BusinessOrder, OrderDTO>();

            CreateMap<BusinessProduct, ProductToUpdate>();
            CreateMap<BusinessOrder, OrderToUpdate>();

            // DataAccess and database layer
            //Orders and OrderDTO
            CreateMap<OrderDTO, Orders>();
            CreateMap<Orders, OrderDTO>();
            CreateMap<ProductToUpdate, Products>();
            CreateMap<OrderToUpdate, Orders>();

            // Requests and Businiess layer
            CreateMap<AddOrderRequest, BusinessOrder>();
            CreateMap<AddProductRequest, BusinessProduct>();

            CreateMap<UpdateProductRequest, BusinessProduct>();
            CreateMap<UpdateOrderRequest, BusinessOrder>();

            CreateMap<SearchOrderQuery, BusinessSearchOrder>();
            CreateMap<SearchProductQuery, BusinessSearchProduct>();

            CreateMap<BaseProductRequest, BusinessProduct>();
        }
    }
}