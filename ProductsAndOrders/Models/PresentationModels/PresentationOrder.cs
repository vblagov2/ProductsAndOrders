using System.Collections.Generic;

namespace ProductsAndOrders.Models.PresentationModels
{
    public class PresentationOrder
    {
        public int OrderId { get; set; }
        public string RecipientName { get; set; }
        public string DestinationCity { get; set; }
        public List<PresentationProduct> Products { get; set; }
    }
}