﻿using System.ComponentModel.DataAnnotations;

namespace ProductsAndOrders.Models.Requests
{
    public class BaseProductRequest
    {
        [Required] public string Name { get; set; }
    }
}
