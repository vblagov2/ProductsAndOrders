using System.ComponentModel.DataAnnotations;

namespace ProductsAndOrders.Models
{
    public class UpdateOrderRequest
    {
        [Required] [MinLength(3)] public string DestinationCity { get; set; }
    }
}