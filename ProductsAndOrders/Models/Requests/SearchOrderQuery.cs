namespace ProductsAndOrders.Models.Requests
{
    public class SearchOrderQuery
    {
        public string RecipientName { get; set; }
    }
}