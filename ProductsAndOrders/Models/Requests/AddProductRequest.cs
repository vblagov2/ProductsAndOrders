using ProductsAndOrders.Models.Requests;
using System.ComponentModel.DataAnnotations;

namespace ProductsAndOrders.Models
{
    public class AddProductRequest : BaseProductRequest
    {
        #region Properties
        [Required] [Range(1, 1000000.00)] public decimal? Price { get; set; }

        #endregion
    }
}