using ProductsAndOrders.Models.Requests;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ProductsAndOrders.Models
{
    public class AddOrderRequest
    {
        #region Properties

        [Required] public string RecipientName { get; set; }
        [Required] public string DestinationCity { get; set; }
        [Required] public List<BaseProductRequest> Products { get; set; }

        #endregion
    }
}