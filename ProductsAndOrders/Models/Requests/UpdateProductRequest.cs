using System.ComponentModel.DataAnnotations;

namespace ProductsAndOrders.Models
{
    public class UpdateProductRequest
    {
        [Required] [Range(1, 1000000.00)] public decimal Price { get; set; }
    }
}