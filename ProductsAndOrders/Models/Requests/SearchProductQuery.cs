namespace ProductsAndOrders.Models.Requests
{
    public class SearchProductQuery
    {
        public string Name { get; set; }
    }
}