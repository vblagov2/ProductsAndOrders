using System.Collections.Generic;

namespace ProductsAndOrders.Models.BusinessModels
{
    public class BusinessOrder
    {
        #region Properties

        public int OrderId { get; set; }
        public string RecipientName { get; set; }
        public string DestinationCity { get; set; }
        public List<BusinessProduct> Products { get; set; }

        #endregion
    }
}