namespace ProductsAndOrders.Models.DTOModels
{
    public class ProductToUpdate
    {
        public int ProductId { get; set; }
        public decimal Price { get; set; }
    }
}