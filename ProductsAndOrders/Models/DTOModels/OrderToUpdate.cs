namespace ProductsAndOrders.Models.DTOModels
{
    public class OrderToUpdate
    {
        public int OrderId { get; set; }
        public string DestinationCity { get; set; }
    }
}