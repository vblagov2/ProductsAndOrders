using System.Collections.Generic;

namespace ProductsAndOrders.Models.DTOModels
{
    public class OrderDTO
    {
        public int OrderId { get; set; }
        public string RecipientName { get; set; }
        public string DestinationCity { get; set; }
        public List<Products> Products { get; set; }

        public OrderDTO()
        {
        }

        public OrderDTO(Orders order, List<Products> products)
        {
            OrderId = order.OrderId;
            RecipientName = order.RecipientName;
            DestinationCity = order.DestinationCity;
            Products = products;
        }
    }
}