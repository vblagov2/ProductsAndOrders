﻿using ProductsAndOrders.Exceptions;
namespace ProductsAndOrders.Models.Exceptions
{
    public class ItemNotFoundException : BaseException
    {
        public ItemNotFoundException(string message) : base(message)
        {
            ErrorCode = ErrorCodes.NotFound;
        }
    }
}
