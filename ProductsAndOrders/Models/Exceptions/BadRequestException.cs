﻿using ProductsAndOrders.Exceptions;

namespace ProductsAndOrders.Models.Exceptions
{
    public class BadRequestException : BaseException
    {
        public BadRequestException(string message) : base(message)
        {
            ErrorCode = ErrorCodes.BadRequest;
        }
    }
}
