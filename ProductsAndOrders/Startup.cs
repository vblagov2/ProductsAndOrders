﻿using AutoMapper;
using IdentityServer;
using IdentityServer4.Configuration;
using IdentityServer4.Quickstart.UI;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductsAndOrders.DataStorage;
using ProductsAndOrders.MiddleWares;
using ProductsAndOrders.Services;
using ProductsAndOrders.Tools;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace ProductsAndOrders
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            Environment = env;
        }

        public IConfiguration Configuration { get; }
        public IHostingEnvironment Environment { get; }

        // This method gets called by the runtime. Use this method to add services to the container.

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddTransient<IOrdersService, OrdersService>();
            services.AddTransient<IProductsService, ProductsService>();
            services.AddTransient<IReportsService, ReportsService>();
            services
                .AddDbContext<LibraryContext>(o =>
                    o.UseSqlServer(Configuration["ConnectionString:StoreDB"]));
            services.AddTransient<IProductsLibraryRepository, ProductsLibraryRepository>();
            services.AddTransient<IOrdersLibraryRepository, OrdersLibraryRepository>();
            services.AddTransient<IReportsLibraryRepository, ReportsLibraryRepository>();
            services.AddTransient<IUnitOfWork, UnitOfWork<LibraryContext>>();
            
            // Auto Mapper Configurations
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddIdentityServer(options => options.UserInteraction = new UserInteractionOptions()
            {
                LogoutUrl = "/account/logout",
                LoginUrl = "/account/login",
                LoginReturnUrlParameter = "returnUrl"
            }).AddTestUsers(TestUsers.Users)
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryApiResources(Config.GetApis())
                .AddInMemoryClients(Config.GetClients())
                .AddDeveloperSigningCredential(false);


            services.AddAuthentication()
            .AddIdentityServerAuthentication(options =>
            {
                options.ApiName = "demo_api"; // required audience of access tokens
                options.RequireHttpsMetadata = false; // dev only!
                options.Authority = "https://test-blahov.azurewebsites.net";
            });

            services.AddAuthorization();

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new Info { Title = "Protected API", Version = "v1" });

                options.AddSecurityDefinition("oauth2", new OAuth2Scheme
                {
                    Flow = "implicit",
                    TokenUrl = "/connect/token",
                    AuthorizationUrl = "/connect/authorize",
                    Scopes = new Dictionary<string, string> { { "demo_api", "Demo API - full access" } }
                });

                options.OperationFilter<AuthorizeCheckOperationFilter>(); // Required to use access token
            });


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseMiddleware<ExceptionsMiddleware>();
            // Swagger JSON Doc
            app.UseSwagger();
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
            app.UseAuthentication();
            app.UseIdentityServer();

            // Swagger UI
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                options.RoutePrefix = string.Empty;

                options.OAuthClientId("demo_api_swagger");
                options.OAuthAppName("Demo API - Swagger");
            });


            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();

            app.UseMvc();
        }

        public class AuthorizeCheckOperationFilter : IOperationFilter
        {
            public void Apply(Operation operation, OperationFilterContext context)
            {
                var authAttributes = context.MethodInfo.DeclaringType.GetCustomAttributes(true)
                .Union(context.MethodInfo.GetCustomAttributes(true))
                .OfType<AuthorizeAttribute>();

                if (authAttributes.Any())
                {
                    operation.Responses.Add("401", new Response { Description = "Unauthorized" });

                    operation.Security = new List<IDictionary<string, IEnumerable<string>>>
                {
                    new Dictionary<string, IEnumerable<string>> {{"oauth2", new[] {"demo_api"}}}
                };
                }
            }
        }
    }
}
